export const downloadSVG = (svg, selector, prefix) => {
  const element = document.querySelector(selector);
  element.download = `${prefix}${Date.now()}.svg`;
  const serializer = new XMLSerializer();
  let source = serializer.serializeToString(svg);
  // add name spaces.
  /* if (!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)) {
    source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
  }
  if (!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)) {
    source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
  } */
  source = `<?xml version="1.0" standalone="no"?>\r\n${source}`;
  const url = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(source)}`;
  element.href = url;
};